﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TDL.Gr_Repository;
using TDL.Models;
//using System.Data.Entity;
//using System.Data;

namespace TDL.Gr_EF
{
    public abstract class Repository<TModel> : IRepository<TModel> where TModel : class
    {
        protected readonly KegContext DatabaseContext;

        public Repository(KegContext context)
        {
            this.DatabaseContext = context;
        }

        //POST
        public void Add(TModel entity)
        {
            DatabaseContext.Set<TModel>().Add(entity);
            DatabaseContext.SaveChanges();
        }

        public TModel Get(int id)
        {
            return DatabaseContext.Set<TModel>().Find(id);
        }

        public PaginatedList<TModel> GetAll(KegiatanParameters kegiatanParameters)
        {
            return PaginatedList<TModel>.ToPagedList(DatabaseContext.Set<TModel>(),
                kegiatanParameters.PageNumber,
                kegiatanParameters.PageSize);
        }

        public void Remove(TModel entity)
        {
            DatabaseContext.Set<TModel>().Remove(entity);
        }

        public TModel Delete(int id)
        {
            var entity = DatabaseContext.Set<TModel>().Find(id);

            DatabaseContext.Set<TModel>().Remove(entity);
            DatabaseContext.SaveChanges();

            return entity;
        }

        public void Update(TModel entity)
        {
            DatabaseContext.Entry(entity).State = EntityState.Modified;
            DatabaseContext.Set<TModel>().Update(entity);
            DatabaseContext.SaveChanges();
        }
    }
}
