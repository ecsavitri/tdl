﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TDL.Models;
using TDL.Gr_Repository;
using System.Data.Entity;

namespace TDL.Gr_EF
{
    public class KegiatanGrRepository : Repository<KegiatanItems>, IKegRepository
    {
        public KegContext KegContext
        {
            get { return DatabaseContext as KegContext; }
        }

        public KegiatanGrRepository(KegContext context) : base(context) { }

        public IEnumerable<KegiatanItems> GetKeg()
        {
            return KegContext.kegiatan.ToList();
        }

        public PaginatedList<KegiatanItems> GetKegDateAsc(KegiatanParameters kegiatanParameters)
        {
            var asc = from t in KegContext.kegiatan select t;
            return PaginatedList<KegiatanItems>.
               ToPagedList(KegContext.kegiatan.OrderBy(t => t.tanggal).ToList(),
               kegiatanParameters.PageNumber,
               kegiatanParameters.PageSize);
        }

        public PaginatedList<KegiatanItems> GetKegDateDesc(KegiatanParameters kegiatanParameters)
        {
            var asc = from t in KegContext.kegiatan select t;
            return PaginatedList<KegiatanItems>.
               ToPagedList(KegContext.kegiatan.OrderByDescending(t => t.tanggal).ToList(),
               kegiatanParameters.PageNumber,
               kegiatanParameters.PageSize);
        }

        public PaginatedList<KegiatanItems> GetKegImp(KegiatanParameters kegiatanParameters)
        {
            var imp = from i in KegContext.kegiatan select i;
            return PaginatedList<KegiatanItems>.
               ToPagedList(KegContext.kegiatan.Where(i => i.imp == true).ToList(),
               kegiatanParameters.PageNumber,
               kegiatanParameters.PageSize);
        }

        public IEnumerable<KegiatanItems> GetStatTrue()
        {
            var stat = from s in KegContext.kegiatan select s;
            return KegContext.kegiatan.Where(s => s.status == true).ToList();
        }

        public PaginatedList<KegiatanItems> GetStatFalse(KegiatanParameters kegiatanParameters)
        {
            var stat = from s in KegContext.kegiatan select s;
            return PaginatedList<KegiatanItems>.
                ToPagedList(KegContext.kegiatan.Where(s => s.status == false).ToList(),
                kegiatanParameters.PageNumber,
                kegiatanParameters.PageSize);
        }

        public PaginatedList<KegiatanItems> GetToday(KegiatanParameters kegiatanParameters)
        {
            var today = from t in KegContext.kegiatan select t;
            return PaginatedList<KegiatanItems>.
                ToPagedList(KegContext.kegiatan.Where(t => t.tanggal.Date == DateTime.Now.Date).OrderBy(t => t.tanggal).ToList(),
                kegiatanParameters.PageNumber,
                kegiatanParameters.PageSize);
        }

        public PaginatedList<KegiatanItems> GetRange(KegiatanParameters kegiatanParameters)
        {
            var datatanggal = from t in KegContext.kegiatan select t;

            return PaginatedList<KegiatanItems>.
                ToPagedList(KegContext.kegiatan.Where(t => t.tanggal.Date >= kegiatanParameters.mindate &&
                              t.tanggal.Date <= kegiatanParameters.maxdate).ToList(),
                kegiatanParameters.PageNumber,
                kegiatanParameters.PageSize);
        }

        public IEnumerable<KegiatanItems> GetSearch(string SearchString)
        {
            var keg = from k in KegContext.kegiatan select k;
            return KegContext.kegiatan.Where(k => k.kegiatan.Contains(SearchString)).ToList();
        }

        public KegiatanItems UStatus(int id)
        {
            var entity = KegContext.kegiatan.AsNoTracking().FirstOrDefault(e => e.id == id);

            if (entity.status == false)

            {
                entity.status = true;
                KegContext.kegiatan.Update(entity);
                KegContext.SaveChanges();
            }

            else
            {
                entity.status = false;
                KegContext.kegiatan.Update(entity);
                KegContext.SaveChanges();
            }

            return entity;
        }

        public KegiatanItems UImp(int id)
        {
            var entity = KegContext.kegiatan.AsNoTracking().FirstOrDefault(e => e.id == id);

            if (entity.imp == false)

            {
                entity.imp = true;
                KegContext.kegiatan.Update(entity);
                KegContext.SaveChanges();
            }

            else
            {
                entity.imp = false;
                KegContext.kegiatan.Update(entity);
                KegContext.SaveChanges();
            }

            return entity;
        }
    }
}
