﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TDL.Models;
using TDL.Gr_EF;
using TDL.Gr_Repository;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;

namespace TDL.Gr_API
{

    [ApiController]
    [EnableCors("MyCORS")]
    public abstract class ParentController<TModel, TRepository> : ControllerBase where TModel : class where TRepository : IRepository<TModel>
    {
        protected readonly TRepository Repository;

        public ParentController(TRepository repository)
        {
            this.Repository = repository;
        }

        [HttpGet]
        public IActionResult Get([FromQuery] KegiatanParameters kegiatanParameters)
        {
            var getkeg = Repository.GetAll(kegiatanParameters);
            var metadata = new
            {
                getkeg.TotalCount,
                getkeg.PageSize,
                getkeg.CurrentPage,
                getkeg.TotalPages,
                getkeg.HasNext,
                getkeg.HasPrev
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            return Ok(getkeg);
        }

        [HttpDelete]
        public void Remove([FromBody] TModel entity)
        {
            Repository.Remove(entity);
        }

        [HttpPut("{id}")]
        public void Update([FromBody] TModel entity)
        {
            Repository.Update(entity);
        }
        

        [HttpPost]
        public void Add([FromBody] TModel entity)
        {
            Repository.Add(entity);
        }
    }
}
