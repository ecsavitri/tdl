﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TDL.Models;
using TDL.Gr_EF;
using TDL.Gr_Repository;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Cors;

namespace TDL.Gr_API
{

    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("MyCORS")]
    public class KegiatanController : ParentController<KegiatanItems, KegiatanGrRepository>
    {
        private readonly ILogger<KegiatanController> Log;

        public KegiatanController(KegiatanGrRepository kegiatanRepository, ILogger<KegiatanController> logger) : base (kegiatanRepository)
        {
            Log = logger;

            Log.LogTrace("This will not be seen because minimum level is debug");
            Log.LogInformation("KEGIATAN CONTROLLER DIJALANKAN");
        }

        [Route("[action]")]
        [HttpGet("asc")]
        public IActionResult DateAsc([FromQuery] KegiatanParameters kegiatanParameters)
        {
            var get = Repository.GetKegDateAsc(kegiatanParameters);
            var metadata = new
            {
                get.TotalCount,
                get.PageSize,
                get.CurrentPage,
                get.TotalPages,
                get.HasNext,
                get.HasPrev
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            Log.LogInformation("SORTIR ASCENDING BERDASARKAN TANGGAL KEGIATAN");
            return Ok(get);
        }

        [Route("[action]")]
        [HttpGet("desc")]
        public IActionResult DateDesc([FromQuery] KegiatanParameters kegiatanParameters)
        {
            var get = Repository.GetKegDateDesc(kegiatanParameters);

            var metadata = new
            {
                get.TotalCount,
                get.PageSize,
                get.CurrentPage,
                get.TotalPages,
                get.HasNext,
                get.HasPrev
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            Log.LogInformation("SORTIR DESCENDING BERDASARKAN TANGGAL KEGIATAN");
            return Ok(get);
        }

        [Route("[action]")]
        [HttpGet("imp")]
        public IActionResult Imp([FromQuery] KegiatanParameters kegiatanParameters)
        {
            var get = Repository.GetKegImp(kegiatanParameters);
            var metadata = new
            {
                get.TotalCount,
                get.PageSize,
                get.CurrentPage,
                get.TotalPages,
                get.HasNext,
                get.HasPrev
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            Log.LogInformation("MENAMPILKAN DATA DENGAN STATUS IMPORTANT");
            return Ok(get);
        }

        [Route("[action]")]
        [HttpGet("done")]
        public IEnumerable<KegiatanItems> StatTrue()
        {
            Log.LogInformation("MENAMPILKAN DATA YANG SUDAH SELESAI DIKERJAKAN");
            return base.Repository.GetStatTrue();

        }

        [Route("[action]")]
        [HttpGet("task")]
        public IActionResult StatFalse([FromQuery] KegiatanParameters kegiatanParameters)
        {
            var get = Repository.GetStatFalse(kegiatanParameters);
            var metadata = new
            {
                get.TotalCount,
                get.PageSize,
                get.CurrentPage,
                get.TotalPages,
                get.HasNext,
                get.HasPrev
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            Log.LogInformation("MENAMPILKAN DATA YANG BELUM DIKERJAKAN");
            return Ok(get);
        }

        [Route("[action]")]
        [HttpGet("today")]
        public IActionResult Today([FromQuery] KegiatanParameters kegiatanParameters)
        {
            var get = Repository.GetToday(kegiatanParameters);
            var metadata = new
            {
                get.TotalCount,
                get.PageSize,
                get.CurrentPage,
                get.TotalPages,
                get.HasNext,
                get.HasPrev
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            Log.LogInformation("MENAMPILKAN DATA HARI INI");
            return Ok(get);
        }

        [Route("[action]")]
        [HttpGet("range")]
        public IActionResult Range([FromQuery] KegiatanParameters kegiatanParameters)
        {
            if (!kegiatanParameters.validrange)
            {

                return BadRequest("Tanggal akhir tidak bisa lebih kecil daripada tanggal awal");
            }
            var get = Repository.GetRange(kegiatanParameters);
            var metadata = new
            {
                get.TotalCount,
                get.PageSize,
                get.CurrentPage,
                get.TotalPages,
                get.HasNext,
                get.HasPrev
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            Log.LogInformation("MENAMPILKAN DATA BERDASARKAN FILTER TANGGAL");
            return Ok(get);
        }

        [Route("[action]")]
        [HttpGet("Search/{SearchString}")]
        public IEnumerable<KegiatanItems> SearchTask(string SearchString)
        {
            Log.LogInformation("MENCARI DATA");
            return base.Repository.GetSearch(SearchString);
        }

        [Route("[action]")]
        [HttpPut("UStatus/{id}")]
        public KegiatanItems UStatus(int id)
        {
            Log.LogInformation("MENGUBAH STATUS MENJADI TRUE/FALSE");
            return base.Repository.UStatus(id);
        }

        [Route("[action]")]
        [HttpPut("UImp/{id}")]
        public KegiatanItems UImp(int id)
        {
            Log.LogInformation("MENGUBAH IMPORTANT MENJADI TRUE/FALSE");
            return base.Repository.UImp(id);
        }

        [HttpGet("{id}")]
        public KegiatanItems Get(int id)
        {
            Log.LogInformation("MENAMPILKAN DATA BERDASARKAN ID");
            return base.Repository.Get(id);
        }


        [HttpDelete("{id}")]
        public KegiatanItems Delete(int id)
        {
            Log.LogInformation("MENGHAPUS DATA YANG DIPILIH");
            return base.Repository.Delete(id);
        }

    }
    
}
