﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TDL.Models;

namespace TDL.Gr_Repository
{
    public interface IRepository<T> where T : class
    {
        void Add (T entity);
        void Update (T entity);
        void Remove(T entity);
        PaginatedList<T> GetAll(KegiatanParameters kegiatanParameters);

        T Get(int id);
        T Delete(int id);
        //void UStatus(T entity);
        //void UImp(T entity);
        //UStatus. UImp, Paging
        //void UStatus(T entity);
        //T UpdateData(int id);
    }
}
