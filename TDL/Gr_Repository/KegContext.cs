﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TDL.Models;

namespace TDL.Gr_Repository
{
    public class KegContext : DbContext
    {
        public KegContext(DbContextOptions<KegContext> options) : base(options)
        {
        
        }

        public DbSet<KegiatanItems> kegiatan { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }
    }
}
