﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TDL.Models;

namespace TDL.Gr_Repository
{
    public interface IKegRepository : IRepository<KegiatanItems>
    {
        IEnumerable<KegiatanItems> GetKeg();
        PaginatedList<KegiatanItems> GetKegDateAsc(KegiatanParameters kegiatanParameters);
        PaginatedList<KegiatanItems> GetKegDateDesc(KegiatanParameters kegiatanParameters);
        PaginatedList<KegiatanItems> GetKegImp(KegiatanParameters kegiatanParameters);
        IEnumerable<KegiatanItems> GetStatTrue();
        PaginatedList<KegiatanItems> GetStatFalse(KegiatanParameters kegiatanParameters);
        PaginatedList<KegiatanItems> GetToday(KegiatanParameters kegiatanParameters);
        IEnumerable<KegiatanItems> GetSearch(string SearchString);
        PaginatedList<KegiatanItems> GetRange(KegiatanParameters kegiatanParameters);

        KegiatanItems UStatus(int id);
        KegiatanItems UImp(int id);
    }
}
