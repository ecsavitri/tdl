﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TDL.Models;

namespace TDL.Controllers
{
    [Route("api/TDL")]
    [ApiController]
    public class TDLController : ControllerBase
    {
        private readonly KegiatanContext _context;

        public TDLController(KegiatanContext context)
        {
            _context = context;
        }

        // GET: api/TDL
        [HttpGet]
        public ActionResult<IEnumerable<KegiatanItems>> Getkegiatanbr(int? pageNumber)
        {
            int pageSize = 10;
            return PaginatedList<KegiatanItems>.ToPagedList(_context.kegiatan.AsNoTracking(), pageNumber ?? 1, pageSize);
        }

        // GET: api/TDL/today
        [HttpGet("today")]
        public ActionResult<IEnumerable<KegiatanItems>> Getkegiatantoday(int? pageNumber)
        {
            var today = from t in _context.kegiatan select t;
            today = today.Where(t => t.tanggal.Date == DateTime.Now.Date).OrderBy(t => t.tanggal);


            int pageSize = 10;
            return PaginatedList<KegiatanItems>.ToPagedList(today.AsNoTracking(), pageNumber ?? 1, pageSize);
        }

        // GET data berdasarkan Search
        [HttpGet("today/{SearchString}")]
        public ActionResult SearchToday(string SearchString, int? pageNumber)
        {
            if (SearchString != null)
            {
                pageNumber = 1;
            }

            var today = from t in _context.kegiatan select t;
            today = today.Where(t => t.tanggal.Date == DateTime.Now.Date);

            if (!String.IsNullOrEmpty(SearchString))
            {
                today = today.Where(k => k.kegiatan.Contains(SearchString));
            }

            int pageSize = 10;
            return Ok(PaginatedList<KegiatanItems>.ToPagedList(today.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET data asc
        [HttpGet("asc")]
        public ActionResult<IEnumerable<KegiatanItems>> Asc(int? pageNumber)
        {

            var asc = from t in _context.kegiatan select t;

            asc = asc.OrderBy(t => t.tanggal);
            int pageSize = 10;
            return PaginatedList<KegiatanItems>.ToPagedList(asc.AsNoTracking(), pageNumber ?? 1, pageSize);
        }

        // GET data desc
        [HttpGet("desc")]
        public ActionResult<IEnumerable<KegiatanItems>> Desc(int? pageNumber)
        {

            var desc = from t in _context.kegiatan select t;

            desc = desc.OrderByDescending(t => t.tanggal);
            int pageSize = 10;
            return PaginatedList<KegiatanItems>.ToPagedList(desc.AsNoTracking(), pageNumber ?? 1, pageSize);
        }
        /*
        [HttpGet]
        [Produces("application/json")]
        public async Task<ActionResult> Getkegiatanbr()
        {
            var result = await _context.kegiatan.ToListAsync();
            return Ok(result);
        }
        } 
             
        */

        // GET: api/TDL/5
        [HttpGet("{id}")]
        public async Task<ActionResult<KegiatanItems>> GetKegiatanItems(int id)
        {
            var kegiatanItems = await _context.kegiatan.FindAsync(id);

            if (kegiatanItems == null)
            {
                return NotFound();
            }

            return kegiatanItems;
        }

        //Get Time Span
        [HttpGet("Interval/{id}")]
        [Produces("application/json")]
        public string Interval(int id, DateTime d)
        {
            var Entity = _context.kegiatan.AsNoTracking().FirstOrDefault(e => e.id == id);
            //var today = from t in _context.kegiatan select t;
            //today = today.Where(t => t.tanggal.Date != null);
            TimeSpan s = DateTime.Now.Subtract(d);
            int dayDiff = (int)s.TotalDays;

            if (dayDiff == 1)
            {
                return "yesterday";
            }
            if (dayDiff < 7)
            {
                return string.Format("{0} days ago", dayDiff);
            }
            if (dayDiff < 31)
            {
                return string.Format("{0} weeks ago", Math.Ceiling((double)dayDiff / 7));
            }

            return null;
        }

        // GET data berdasarkan Search
        [HttpGet("Search/{SearchString}")]
        public IActionResult Search(string SearchString, int? pageNumber)
        {
            if (SearchString != null)
            {
                pageNumber = 1;
            }

            var keg = from k in _context.kegiatan select k;

            if (!String.IsNullOrEmpty(SearchString))
            {
                keg = keg.Where(k => k.kegiatan.Contains(SearchString));
            }

            int pageSize = 10;
            return Ok(PaginatedList<KegiatanItems>.ToPagedList(keg.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET data Status true
        [HttpGet("done")]
        public IActionResult Status()
        {

            var stat = from s in _context.kegiatan select s;

            stat = stat.Where(s => s.status == true);

            return Ok(stat.AsNoTracking().ToList());
        }

        // GET data Status false
        [HttpGet("task")]
        public IActionResult StatusNot(int? pageNumber)
        {

            var stat = from s in _context.kegiatan select s;

            stat = stat.Where(s => s.status == false);
            int pageSize = 10;
            return Ok(PaginatedList<KegiatanItems>.ToPagedList(stat.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET data berdasarkan Search
        [HttpGet("task/{SearchString}")]
        public IActionResult SearchPlanned(string SearchString, int? pageNumber)
        {
            if (SearchString != null)
            {
                pageNumber = 1;
            }

            var stat = from s in _context.kegiatan select s;

            stat = stat.Where(s => s.status == false);

            if (!String.IsNullOrEmpty(SearchString))
            {
                stat = stat.Where(k => k.kegiatan.Contains(SearchString));
            }

            int pageSize = 10;
            return Ok( PaginatedList<KegiatanItems>.ToPagedList(stat.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET data Important
        [HttpGet("imp")]
        public IActionResult Imp(int? pageNumber)
        {

            var imp = from i in _context.kegiatan select i;

            imp = imp.Where(i => i.imp == true);
            int pageSize = 10;

            return Ok(PaginatedList<KegiatanItems>.ToPagedList(imp.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        [HttpGet("imp/{SearchString}")]
        public IActionResult SearchImp(string SearchString, int? pageNumber)
        {
            if (SearchString != null)
            {
                pageNumber = 1;
            }

            var imp = from i in _context.kegiatan select i;

            imp = imp.Where(i => i.imp == true);

            if (!String.IsNullOrEmpty(SearchString))
            {
                imp = imp.Where(k => k.kegiatan.Contains(SearchString));
            }

            int pageSize = 10;
            return Ok(PaginatedList<KegiatanItems>.ToPagedList(imp.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        // GET data Unimportant
        [HttpGet("unimp")]
        public IActionResult Unimp()
        {

            var imp = from i in _context.kegiatan select i;

            imp = imp.Where(i => i.imp == false);

            return Ok(imp.AsNoTracking().ToList());
        }

        [HttpPut("UStatus/{id}")]
        [Produces("application/json")]
        public IActionResult UStatus(int id)
        {
            var Entity = _context.kegiatan.AsNoTracking().FirstOrDefault(e => e.id == id);

            if (Entity.status == false)

            {
                Entity.status = true;
                _context.kegiatan.Update(Entity);
                _context.SaveChanges();
            }

            else
            {
                Entity.status = false;
                _context.kegiatan.Update(Entity);
                _context.SaveChanges();
            }
            //CreatedAtRoute("Ustatus", new { id = kegiatanItems.id }, kegiatanItems)
            return NoContent();
        }

        [HttpPut("UImp/{id}")]
        [Produces("application/json")]
        public IActionResult UImp(int id)
        {
            var Entity = _context.kegiatan.AsNoTracking().FirstOrDefault(e => e.id == id);

            if (Entity.imp == false)

            {
                Entity.imp = true;
                _context.kegiatan.Update(Entity);
                _context.SaveChanges();
            }

            else
            {
                Entity.imp = false;
                _context.kegiatan.Update(Entity);
                _context.SaveChanges();
            }
            return NoContent();
        }

        // PUT: api/TDL/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutKegiatanItems(int id, KegiatanItems kegiatanItems)
        {
            if (id != kegiatanItems.id)
            {
                return BadRequest();
            }

            _context.Entry(kegiatanItems).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KegiatanItemsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TDL
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<KegiatanItems>> PostKegiatanItems(KegiatanItems kegiatanItems)
        {

            _context.kegiatan.Add(kegiatanItems);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetKegiatanItems", new { id = kegiatanItems.id }, kegiatanItems);
        }

        // DELETE: api/TDL/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<KegiatanItems>> DeleteKegiatanItems(int id)
        {
            var kegiatanItems = await _context.kegiatan.FindAsync(id);
            if (kegiatanItems == null)
            {
                return NotFound();
            }

            _context.kegiatan.Remove(kegiatanItems);
            await _context.SaveChangesAsync();

            return kegiatanItems;
        }

        private bool KegiatanItemsExists(int id)
        {
            return _context.kegiatan.Any(e => e.id == id);
        }
    }
}
