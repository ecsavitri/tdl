﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TDL.Data.EFCore;
using TDL.Models;

namespace TDL.Controllers
{
    
    [Route("api/TDLRepos")]
    [ApiController]
    public class KegController : TDLRepoController<KegiatanItems, EFCoreKegiatanRepository>
    {
        private static KegiatanContext context;

        public KegController(EFCoreKegiatanRepository repository) : base(repository, context)
        {

        }
    }
    
}