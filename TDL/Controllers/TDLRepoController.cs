﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TDL.Data;
using TDL.Models;
using TDL.Data.EFCore;

namespace TDL.Controllers
{
    [Route("api/TDLRepo")]
    [ApiController]
    public abstract class TDLRepoController<TEntity, TRepository> : ControllerBase
        where TEntity : class, IEntity
        where TRepository : IRepository<TEntity>
    {
        private readonly TRepository repository;
        private readonly KegiatanContext context;

        public TDLRepoController(TRepository repository, KegiatanContext context)
        {
            this.repository = repository;
            this.context = context;
        }


        // GET: api/[controller]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TEntity>>> Get()
        {
            return await repository.GetAll();
        }
        
        
        // GET: api/[controller]
        [HttpGet("today")]
        public async Task<ActionResult<IEnumerable<TEntity>>> GetAll()
        {
            var today = from t in context.kegiatan select t;
            today = today.Where(t => t.tanggal.Date == DateTime.Now.Date).OrderBy(t => t.tanggal);

            return await repository.GetAll();
        }
       

        // GET: api/[controller]/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TEntity>> Get(int id)
        {
            var keg = await repository.Get(id);
            if (keg == null)
            {
                return NotFound();
            }
            return keg;
        }

        // PUT: api/[controller]/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, TEntity keg)
        {
            if (id != keg.id)
            {
                return BadRequest();
            }
            await repository.Update(keg);
            return NoContent();
        }

        // POST: api/[controller]
        [HttpPost]
        public async Task<ActionResult<TEntity>> Post(TEntity keg)
        {
            await repository.Add(keg);
            return CreatedAtAction("Get", new { id = keg.id }, keg);
        }

        // DELETE: api/[controller]/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TEntity>> Delete(int id)
        {
            var keg = await repository.Delete(id);
            if (keg == null)
            {
                return NotFound();
            }
            return keg;
        }

    }
}