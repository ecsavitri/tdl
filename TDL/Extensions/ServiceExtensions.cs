﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection; //untuk IServiceCollection
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder; // untik IISOptions

namespace TDL.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });
        }

        public static void ConfigureIISConfiguration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
            });
        }
        /*
        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
        }
        */
    }
}
