using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using TDL.Models;
using System.Web.Http.Cors;
using Microsoft.OpenApi.Models;
using TDL.Data.EFCore;
using TDL.Extensions;
//using TDL.DataAccessLayer;
using TDL.Data;
using AutoMapper;
using TDL.Gr_Repository;
using TDL.Gr_EF;
using Microsoft.AspNetCore.Mvc.Versioning;

namespace TDL
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureCors();
            services.ConfigureIISConfiguration();
            
            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
                o.ApiVersionReader = new HeaderApiVersionReader("api-version");
            });
            
            services.AddResponseCompression();
            services.AddCors();

            services.AddSwaggerGen( c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API TDL", Version = "v1" });
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            });

            //Gr_
            services.AddDbContext<KegContext>(opt =>
               opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<KegContext>(opt => opt.UseInMemoryDatabase("kegiatan"));
            services.AddScoped<IKegRepository, KegiatanGrRepository>();
            services.AddScoped<KegiatanGrRepository>();

            services.AddDbContext<KegiatanContext>(opt =>
               opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<EFCoreKegiatanRepository>();
            services.AddAutoMapper(typeof(Startup));
            services.AddDbContext<KegiatanContext>(opt => opt.UseInMemoryDatabase("kegiatan"));
            services.AddControllers();
        }

        private void SetMigrate(string DefaultConnection)
        {
            var optionsBuilder = new DbContextOptionsBuilder<KegContext>();
            optionsBuilder.UseSqlServer(DefaultConnection);
            using (var context = new KegContext(optionsBuilder.Options))
            {
                context.Database.Migrate();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().WithExposedHeaders("X-Pagination"));

            app.UseResponseCompression();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            ///app.UseCors("CorsPolicy");

            /*
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All
            });
            */

            app.UseRouting();

            app.UseAuthorization();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "TDL/swagger/Kegiatan/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("TDL/swagger/v1/swagger.json", "TestService");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
