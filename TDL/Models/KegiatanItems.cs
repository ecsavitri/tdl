﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TDL.Data;

namespace TDL.Models
{
    public class KegiatanItems : IEntity
    {
        public int id { get; set; }
         
        [StringLength(200, ErrorMessage= "Kegiatan tidak bisa lebih dari 200 karakter")]
        [Required(ErrorMessage = "Kegiatan harus diisi")]
        public string kegiatan { get; set; }

        //[DataType(DataType.Date)]
        [Required(ErrorMessage = "Tanggal harus diisi")]
        public DateTime tanggal { get; set; }

        [StringLength(200, ErrorMessage = "Keterangan tidak bisa lebih dari 200 karakter")]
        public string ket { get; set; }

        public bool status { get; set; }

        public bool imp { get; set; }

    }

    public class KegiatanItemsDTO
    {
        public string indays { get; set; }
    }
}
