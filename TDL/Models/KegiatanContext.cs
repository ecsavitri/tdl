﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TDL.Models
{
    public class KegiatanContext : DbContext
    {
        public KegiatanContext()
        {

        }
        public KegiatanContext(DbContextOptions<KegiatanContext> options)
            : base(options)
        {

        }

        public DbSet<KegiatanItems> kegiatan { get; set; }

    }
}
