﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations; // untuk DataType


namespace TDL.Models
{
    public class KegiatanParameters
    {
        const int maxPageSize = 50;
        public int PageNumber { get; set; } = 1;

        private int pageSize = 10;
        public int PageSize
        {
            get
            {
                return pageSize;
            }
            set
            {
                pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        [DataType(DataType.Date)]
        public DateTime mindate { get; set; }

        [DataType(DataType.Date)]
        public DateTime maxdate { get; set; } = (DateTime)DateTime.Now.Date;

        public bool validrange => maxdate > mindate;
    }
}
