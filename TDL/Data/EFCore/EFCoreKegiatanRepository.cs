﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TDL.Models;
using TDL.Data.EFCore;
using Microsoft.EntityFrameworkCore;

namespace TDL.Data.EFCore
{
    public class EFCoreKegiatanRepository : EFCoreRepository<KegiatanItems, KegiatanContext>
    {
        public EFCoreKegiatanRepository(KegiatanContext context) : base (context)
        {

        }

    }
}
