﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDL.Data
{
    public interface IEntity
    {
        int id { get; set; }
    }
}
