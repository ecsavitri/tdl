﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using System.Data;
using TDL.Models;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;

namespace TDL.DataAccessLayer
{
    public class KegiatanRepository : IKegiatanRepository, IDisposable
    {
        
        public KegiatanContext context;

        public KegiatanRepository(KegiatanContext context)
        {
            this.context = context;
        }

        public async Task<List<KegiatanItems>> GetKeg()
        {
            return await context.Set<KegiatanItems>().ToListAsync();
        }

        public async Task<KegiatanItems> GetKegByID(int id)
        {
            return await context.Set<KegiatanItems>().FindAsync(id);
        }

        public async Task<KegiatanItems> InsertKeg(KegiatanItems kegiatan)
        {
            context.Set<KegiatanItems>().Add(kegiatan);
            await context.SaveChangesAsync();
            return kegiatan;
        }

        public async Task<KegiatanItems> DeleteKeg(int id)
        {
            var entity = await context.Set<KegiatanItems>().FindAsync(id);
            if (entity == null)
            {
                return entity;
            }

            context.Set<KegiatanItems>().Remove(entity);
            await context.SaveChangesAsync();

            return entity;
        }

        public async Task<KegiatanItems> UpdateKeg(KegiatanItems kegiatan)
        {
            context.Entry(kegiatan).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return kegiatan;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
