﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TDL.Models;

namespace TDL.DataAccessLayer
{
    public interface IKegiatanRepository : IDisposable
    {
        Task<List<KegiatanItems>> GetKeg();
        Task<KegiatanItems> GetKegByID(int id);
        Task<KegiatanItems> InsertKeg(KegiatanItems kegiatan);
        Task<KegiatanItems> UpdateKeg(KegiatanItems kegiatan);
        Task<KegiatanItems> DeleteKeg(int id);
        void Save();
        //void AsNoTracking();
    }
}
